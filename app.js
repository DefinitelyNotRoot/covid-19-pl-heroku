const express = require('express');
const app = express();
const puppeteer = require('puppeteer');
const port = process.env.PORT || 8080;

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", '*');
    res.header("Access-Control-Allow-Credentials", true);
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
    next();
});



app.get('/', function(req, res, next) {
    const url = 'https://www.gov.pl/web/koronawirus/wykaz-zarazen-koronawirusem-sars-cov-2'
    console.log('Openining:', url);
    (async() => {
        const browser = await puppeteer.launch({
            args: ['--no-sandbox',
                '--disable-setuid-sandbox'
            ]
        });
        const page = await browser.newPage();
        await page.goto(url);
        await page.click('#cookies-info > div > button');
        await page.click('#js-pagination-rows > a:nth-child(2)')
        const table_data = await page.evaluate(() => {

            const query = (selector, context) =>
                Array.from(context.querySelectorAll(selector));
            return query('tr', document).map(row =>
                query('td', row).map(cell =>
                    cell.textContent))
        });
        table_data.splice(-2, 2)
        table_data.shift()
        var xd = new Object();
        var headers = ['Wojewodztwo', 'Zachorowan', 'Zgonow']
        for (var i = 0; i < table_data.length; i++) {
            table_data[i].pop()
        }

        function createItem(item) {
            newObject = {}
            item.map((val, i) => newObject[headers[i]] = isNaN(val) ? val : parseInt(val) || 0)
            return newObject
        }
        let output = table_data.map(item => createItem(item));
        res.setHeader('Content-Type', 'application/json');
        res.send(output)

        await browser.close();
    })();


});

app.listen(port, function() {
    console.log('App listening on port ' + port)
})